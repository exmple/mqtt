package org.example.mqtt.demo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.annotation.IntegrationComponentScan;

@SpringBootApplication
@IntegrationComponentScan
public class Demo1Application {


    public static void main(final String... args) {
        SpringApplication.run(Demo1Application.class, args);
    }


}
