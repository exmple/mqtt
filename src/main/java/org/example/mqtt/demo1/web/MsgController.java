package org.example.mqtt.demo1.web;

import org.example.mqtt.demo1.message.MqttSender;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by way on 2017/12/22.
 */
@RestController
public class MsgController {

    @Resource
    private MqttSender mqttSender;

    @RequestMapping(value = "/messages", method = RequestMethod.POST)
    public void publish(@RequestBody String message) {
        mqttSender.send(message);
    }

}
