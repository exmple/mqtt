package org.example.mqtt.demo1.message;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

/**
 * Created by way on 2017/12/22.
 */
@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
public interface MqttSender {

    void send(String data);

    void send(Message message);
}
