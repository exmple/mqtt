# mqtt
mqtt demo


协议

https://mcxiaoke.gitbooks.io/mqtt-cn/content/

topic设计

- 服务器订阅 server/#

- 客户端发送消息为 server/uuid(客户端唯一标识)

- 服务器广播发送 client

- 服务器单播发送 client/uuid(客户端唯一标识)

- 客户端订阅  client和client/uuid

# 实现广播和单播
（client1发上来的，发回给client1）


## 演示
初始化一个server 2个client
![init](doc/image/01-init.png)

- 广播

调用8888server广播发送消息

![servermsg](doc/image/02-servermsg.png)

client1 client2分别收到消息

![clientreceive](doc/image/02-clientreceive.png)

- 单播

调用8881client1发送消息，服务器收到消息后回只回给client1

![client1send](doc/image/03-client1msg.png)

![serverreceiveclient1](doc/image/03-serverreceive.png)

![clientreceive](doc/image/03-clientreceive.png)






# 代办的
- [x] 服务器简单收发信息
- [x] 服务器收取所有NB信息
- [x] 服务器发给指定的机器
- [ ] 服务器收取消息后根据类型做不同处理
- [x] 为什么一个收到消息了，另外一个会报lost connection 

因为 发消息和收消息的clientId设置一样了，或者与其他clientId重复了。
clientId 我这里采用uuid,发和收加一个前缀。
client发出去的消息topic为"client/uuid"


- [x] 模拟客户端发消息

- [ ] netty版实线

- [ ] netty版和spring版比较

# 物联网资料

有人开发的透传云，工业设备采集/报表

http://cloud.usr.cn/development_instruction.html 

国外的thingsboard,github上star1000，开源的Iot 平台 

https://thingsboard.io/

thingsboard性能翻译

http://www.jkeabc.com/636250.html

nb-iot lora对比

http://tech.sina.com.cn/roll/2017-09-23/doc-ifymeswc9277392.shtml
https://zhuanlan.zhihu.com/p/27470052

nb-iot 软肋

http://bbs.elecfans.com/jishu_1130879_1_1.html


NB-IOT之一个完整的BC95 UDP从开机到数据发送接收过程

http://blog.csdn.net/iotclub/article/details/78128589?locationNum=7&fps=1

nb-iot udp协议和coap协议

https://www.jianshu.com/p/3fde8a19810d

nb-iot 术语

http://www.sohu.com/a/121079130_467823

nb-iot 百科

https://baike.baidu.com/item/NB-IoT/19420464?fr=aladdin

华为nb-iot 

http://developer.huawei.com/ict/cn/site-iot/product/nb-iot
